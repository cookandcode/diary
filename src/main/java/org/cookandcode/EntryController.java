package org.cookandcode;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
public class EntryController {

    @Autowired
    private EntryRepository entryRepository;

    @GetMapping("/api/diary")
    public List<Entry> index() {
        return entryRepository.findAll();
    }

    @PostMapping("/api/diary")
    public List<Entry> store(@RequestBody EntryDTO entryDTO) {
        entryRepository.save(new Entry(entryDTO.getText(), entryDTO.getHeadline()));
        return index();
    }

    @GetMapping("/api/diary/{id}")
    public Entry show(@PathVariable("id") long id) {
        Entry response = entryRepository.findById(id).get();
        return response;
    }

    @PutMapping("/api/diary/{id}")
    public Entry update(@PathVariable("id") long id, @RequestBody EntryDTO entryDTO) {
        Entry entry = entryRepository.findById(id).get();
        entry.setText(entryDTO.getText());
        entry.setHeadline(entryDTO.getHeadline());
        entryRepository.save(entry);
        return entry;
    }

    @DeleteMapping("/api/diary/{id}")
    public List<Entry> destroy(@PathVariable("id") long id) {
        Entry entry = entryRepository.findById(id).get();
        entryRepository.delete(entry);
        return index();
    }

}
